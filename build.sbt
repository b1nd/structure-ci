ThisBuild / name         := "structure"
ThisBuild / version      := "0.1"
ThisBuild / organization := "ru.tinkoff"

lazy val structure = project
  .in(file("."))
  .settings(Settings.Common)
  .settings(Settings.Scalafix)
  .settings(Settings.Assembly)
  .settings(
    Compile / mainClass := Some("ru.tinkoff.structure.App"),
    libraryDependencies ++= Dependency.All ++ Dependency.Tests
  )
