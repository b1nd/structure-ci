package ru.tinkoff.structure.user.service

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import ru.tinkoff.structure.user.domain.{BadUser, CoolUser}

class UserServiceTest extends AnyFlatSpec with Matchers {

  it should "return logic 1 on CoolUser" in new {
    val result = UserService.printUser(CoolUser)
    result shouldBe "logic 1"
  }

  it should "return logic 2 on CoolUser" in new {
    val result = UserService.printUser(BadUser)
    result shouldBe "logic 2"
  }
}
