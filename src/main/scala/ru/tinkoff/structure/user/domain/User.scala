package ru.tinkoff.structure.user.domain

sealed trait User
case object CoolUser    extends User
case object BadUser     extends User
case object AnotherUSer extends User
