package ru.tinkoff.structure.user.service

import ru.tinkoff.structure.user.domain.{AnotherUSer, BadUser, CoolUser, User}

object UserService {
  def printUser(user: User): String = user match {
    case CoolUser    => "logic 1"
    case BadUser     => "logic 2"
    case AnotherUSer => "logic 3"
  }
}
