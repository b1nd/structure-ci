package ru.tinkoff.structure

import ru.tinkoff.structure.config.AppConfigParser
import ru.tinkoff.structure.user.domain.{AnotherUSer, User}
import ru.tinkoff.structure.user.service.UserService

object App {
  def main(args: Array[String]): Unit = {
    val config = AppConfigParser.read
    println(config)

    val user: User = AnotherUSer

    val _ = UserService.printUser(user)
  }
}
