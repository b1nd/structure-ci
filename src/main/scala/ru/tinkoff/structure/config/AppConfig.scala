package ru.tinkoff.structure.config

import com.typesafe.config.ConfigFactory
import pureconfig.ConfigSource

case class Inner(age: Int)

case class AppConfig(
  inner: Inner,
  size: Int,
  name: String
)

object AppConfigParser {
  def read: AppConfig = {
    import pureconfig.generic.auto._
    val config = ConfigFactory.load()
    ConfigSource.fromConfig(config).loadOrThrow[AppConfig]
  }
}
