import sbt.Keys._
import sbt._
import sbtassembly.AssemblyKeys.{assembly, assemblyJarName, assemblyMergeStrategy, assemblyOutputPath}
import sbtassembly.{MergeStrategy, PathList}
import scalafix.sbt.ScalafixPlugin.autoImport.scalafixSemanticdb

object Settings {
  val Common   = Seq(
    scalaVersion := Dependency.Version.Scala,
    scalacOptions ++= Compiler.Options
  )
  val Scalafix = Seq(
    semanticdbEnabled := true,
    semanticdbVersion := scalafixSemanticdb.revision
  )
  val Assembly = Seq(
    assembly / assemblyOutputPath    := target.value / (assembly / assemblyJarName).value,
    assembly / assemblyMergeStrategy := {
      case PathList("META-INF", _) => MergeStrategy.discard
      case _                       => MergeStrategy.first
    }
  )
}
