import sbt._

object Dependency {
  object Version {
    val Scala      = "2.13.6"
    val pureconfig = "0.17.0"
    val scalatest  = "3.2.9"
    val mockito    = "1.16.37"
  }

  val pureconfig = "com.github.pureconfig" %% "pureconfig" % Version.pureconfig

  val scalatest = "org.scalatest" %% "scalatest"     % Version.scalatest % Test
  val mockito   = "org.mockito"   %% "mockito-scala" % Version.mockito   % Test

  val All = Seq(pureconfig)

  val Tests = Seq(scalatest, mockito)
}
